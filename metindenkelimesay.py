# -*- coding: utf-8 -*-

import codecs
from pyspark.sql.functions import regexp_replace, trim, col, lower
from pyspark.sql.functions import split, explode
from pyspark.sql.functions import lit, concat
from pyspark.sql.functions import desc
from pyspark.sql import SQLContext
from pyspark.sql import Row

def kelimesay(kparametre):		# belgede hangi kelimeden kaç tane geçtiğini bulmak için fonksiyon

    say = (kparametre.groupBy(['word']).count())
    return say

def temizleme(tparametre):		#belgedeki büyük harfleri temizlemek,boşlukları temizlemek,küçük harf yapmak için fonksiyon

    col1 = regexp_replace(tparametre, '[^A-Za-z0-9ığüşöçİĞÜŞÖÇ ]+', '')
    
    col2 = trim(col1)
    
    col3 = lower(col2)
    
    return col3

belge = raw_input("dosya yolu giriniz \n")		

belgedf = sqlContext.read.text(belge).select(temizleme(col('value')))		#temizleme fonksiyonunu uyguluyoruz.

belgedf1 = (belgedf.withColumnRenamed('lower(trim(regexp_replace(value,[^A-Za-z0-9ığüşöçİĞÜŞÖÇ ]+,)))', 'yeniisim')) # yeni isim verme örneği

kelimeler = (belgedf1.select(explode(split('yeniisim', '[" "]')).alias('word'))).filter('word != ""') # satırsal boşlukları silme ve kelimeleri ayırma

kelimevesayi = kelimesay(kelimeler).orderBy(desc("count"))	# kelimelere kelimesay fonksiyonu uygulayarak saydırıyoruz ve count a göre azalan bir şekilde sıralama yapıyoruz.

kelimevesayi.show()

liste = []
for i in kelimevesayi.collect():
	liste.append(i)

yol = raw_input("Kaydedilecek dizin ve dosya adı \n")

dosya = codecs.open(yol, "w", "utf-8")

for eleman in liste:
	print>>dosya, eleman[0], eleman[1]
