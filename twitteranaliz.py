# -*- coding: utf-8 -*-

import csv
import tweepy
import codecs
import threading
import time
import re
import matplotlib.pyplot as plt
import sys
from pyspark.sql.functions import regexp_replace, trim, col, lower
from pyspark.sql.functions import split, explode
from pyspark.sql.functions import lit, concat
from pyspark.sql.functions import desc
from pyspark.sql import SQLContext
from pyspark.sql import Row

reload(sys)
sys.setdefaultencoding('utf-8')

##########################  Tweepy Kısmı Başlangıç ############################

dosya3 = codecs.open("/home/fatih/Desktop/sharp/sonuclar.csv", "a", "utf-8")
writer = csv.writer(dosya3, quoting=csv.QUOTE_NONNUMERIC)
writer.writerow( ('Tarih', 'Kelime', 'NormalDurum', 'YeniDurum', 'Analiz') )


sayac = 0
def basla():

	global sayac

	global t

	t = threading.Timer(60.0, basla)

	t.start()

	consumer_key = '9Vt92TJcVCairDVBIPgcRB2Bh'
	consumer_secret = 'f4C20BdRvmHyZR39MuIWwA1H9ZcL7cenFJQs4SKH9cXgW3uzjR'
	access_token = '736980159878823936-nyUDyNueAmIZKzQId5H6fG0Ch2ELZHG'
	access_token_secret = 'j9Dm1gf8YwLRJFLYfEfbB8cvlcRj4wVJvMAZym78OuV95'
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_token_secret)
	api = tweepy.API(auth)


	liste = []

	tweetler = tweepy.Cursor(api.search, q='#EURO2016').items(100)

	veri = []
	
	with codecs.open('/home/fatih/Desktop/sharp/kelimeler/kelimeler.txt', 'r+', encoding='utf8') as veridosya:
		reader1 = csv.reader(veridosya, delimiter=' ', quoting=csv.QUOTE_NONE)
		for row in reader1:
			veri.append(row)

	for tweet in tweetler:
		liste.append(tweet.text)

	dosya = codecs.open("/home/fatih/Desktop/sharp/tweetler/tweetler"+str(sayac)+".txt", "w", "utf-8")

	for eleman in liste:
		print>>dosya, eleman

##########################  Tweepy Kısmı Son ############################


##########################  Spark Kısmı Başlangıç  ######################

	def kelimesay(kparametre):

		say = (kparametre.groupBy(['word']).count())

		return say

	def temizleme(tparametre):

		col1 = regexp_replace(tparametre, '[^A-Za-z0-9ığüşöçİĞÜŞÖÇ ]+', '')

		col2 = trim(col1)

		col3 = lower(col2)

		return col3		

	belgedf = sqlContext.read.text("/home/fatih/Desktop/sharp/tweetler/tweetler"+str(sayac)+".txt").select(temizleme(col('value')))

	belgedf1 = (belgedf.withColumnRenamed('lower(trim(regexp_replace(value,[^A-Za-z0-9ığüşöçİĞÜŞÖÇ ]+,)))', 'yeniisim'))

	kelimeler = (belgedf1.select(explode(split('yeniisim', '[" "]')).alias('word'))).filter('word != ""') 

	kelimevesayi = kelimesay(kelimeler).orderBy(desc("count"))	

	print time.strftime("%d/%m/%Y - %H:%M:%S Zamanına ait en çok kullanılanlar \n")
	kelimevesayi.show()

	liste1 = []

	for tw in kelimevesayi.collect():
		liste1.append(tw)

	dosya1 = codecs.open("/home/fatih/Desktop/sharp/kelimeler/kelimeler"+str(sayac)+".txt", "w", "utf-8")

	dosya2 = codecs.open("/home/fatih/Desktop/sharp/sonuclar.txt", "a", "utf-8")
	
	dosya3 = codecs.open("/home/fatih/Desktop/sharp/sonuclar.csv", "a", "utf-8")
	writer = csv.writer(dosya3, quoting=csv.QUOTE_NONNUMERIC)

	for eleman1 in liste1:
		print>>dosya1, eleman1[0], eleman1[1]


	uzunlukveri = len(veri)
	uzunlukliste = len(liste1)
	
	print>>dosya2, time.strftime("%d/%m/%Y - %H:%M:%S zamanına ait istatistikler \n")
	
	for x in range(0,uzunlukliste):
		for y in range(0,uzunlukveri):
			if liste1[x][0] == veri[y][0]:
				if int(liste1[x][1]) == int(veri[y][1]):
					print liste1[x][0], "kelimesinde değişiklik yok"
					print>>dosya2, liste1[x][0], "kelimesinde değişiklik yok"
					writer.writerow((time.strftime("%d/%m/%Y-%H:%M:%S"), liste1[x][0], veri[y][1], liste1[x][1], "0"))
				if int(liste1[x][1]) < int(veri[y][1]):
					print liste1[x][0], "kelimesinde düşüş var", veri[y][1], "den", liste1[x][1], "e düşüş"
					print>>dosya2, liste1[x][0], "kelimesinde düşüş var", veri[y][1], "den", liste1[x][1], "e düşüş"
					writer.writerow((time.strftime("%d/%m/%Y-%H:%M:%S"), liste1[x][0], veri[y][1], liste1[x][1], "-1"))
				if int(liste1[x][1]) > int(veri[y][1]):
					print liste1[x][0], "kelimesinde artış var", veri[y][1], "den", liste1[x][1], "e artış"
					print>>dosya2, liste1[x][0], "kelimesinde artış var", veri[y][1], "den", liste1[x][1], "e artış"
					writer.writerow((time.strftime("%d/%m/%Y-%H:%M:%S"), liste1[x][0], veri[y][1], liste1[x][1], "1"))
	print "#######################################"
	print sayac, ". AŞAMA BİTTİ"
	print "#######################################"
	print>>dosya2, "#######################################"
	print>>dosya2, sayac, ". AŞAMA BİTTİ"
	print>>dosya2, "#######################################"

##########################  Spark Kısmı Son  ######################
	sayac = sayac + 1




def cizdir(cizkelime):

	cizliste = []
	with codecs.open('/home/fatih/Desktop/sharp/sonuclar.csv', 'r+', encoding='utf8') as cizdosya:
		readerciz = csv.reader(cizdosya)
		next(readerciz, None)
		for stn in readerciz:
			cizliste.append(stn)		
	
	uzunlukciz = len(cizliste)
	arananliste = []
	for i in range(0,uzunlukciz):
		if cizkelime == cizliste[i][1]:
			arananliste.append((cizliste[i][0], cizliste[i][3]))
			
	plt.plot([j for j in range(0,len(arananliste))], [i[1] for i in arananliste], [j for j in range(0,len(arananliste))], [i[1] for i in arananliste], "or")
	x = [j for j in range(0,len(arananliste))]
	plt.xticks(x, [i[0][11:] for i in arananliste], rotation=270, fontsize=9)
	
	plt.axis("equal")
	plt.grid(True)
	plt.xlabel('Zaman')
	plt.ylabel('Kullanim')
	plt.show()

basla()

